resource "google_container_cluster" "demo_cluster" {
  provider = google-beta
  name               = "demo-cluster"
  location           = var.zone

  initial_node_count = 4

  network = data.google_compute_network.vpc_network.name

  subnetwork = data.google_compute_subnetwork.demo_subnet.name

  ip_allocation_policy{
    //this block turns on vpc native mode you can specify specific subnets or cidrs for the node and services subnet, or accept the defaults here
    //note secondary ranges are returned in the order created on the subnet so this might seem a little wonky
    cluster_secondary_range_name = data.google_compute_subnetwork.demo_subnet.secondary_ip_range[1].range_name
    services_secondary_range_name = data.google_compute_subnetwork.demo_subnet.secondary_ip_range[0].range_name
  }

  node_config {
    service_account = google_service_account.demo-sa.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    image_type = "cos_containerd"
    machine_type = "e2-standard-4"
    disk_size_gb = "100"

    metadata = {
      ssh-keys       = "gcedemo:${file("gcedemo.pub")}"
    }
  }

  private_cluster_config {
    enable_private_nodes = "true"
    enable_private_endpoint = "true"
    master_ipv4_cidr_block = "10.14.0.0/28"
  }

  master_authorized_networks_config {
    cidr_blocks {
      cidr_block = data.google_compute_subnetwork.demo_subnet.ip_cidr_range
    }
  }

  workload_identity_config {
    workload_pool = "${var.project_id}.svc.id.goog"
  }

  resource_labels = { 
    "mesh_id" : "proj-${data.google_project.project.number}" 
  }

}

output "cluster_zone" {
 value = google_container_cluster.demo_cluster.location
}

output "cluster_name" {
 value = google_container_cluster.demo_cluster.name
}
