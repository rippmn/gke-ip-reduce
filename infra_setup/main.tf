provider "google-beta" {
  project     = var.project_id
  region      = var.region
  zone   = var.zone
}


data "google_project" "project" {
    provider = google-beta
    project_id = var.project_id
}

