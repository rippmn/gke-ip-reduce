resource "google_compute_router" "router" {
  name    = "my-router"
  network = data.google_compute_network.vpc_network.name
  region  = var.region
  project = var.project_id
}

resource "google_compute_router_nat" "nat" {
  name                               = "my-router-nat"
  router                             = google_compute_router.router.name
  region                             = google_compute_router.router.region
  project 			     = var.project_id
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

}

resource "google_compute_instance" "nginx" {
  name         = "nginx-host"
  machine_type = "e2-micro"
  zone         = var.zone
  project      = var.project_id

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = data.google_compute_subnetwork.demo_subnet.self_link
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email = google_service_account.demo-sa.email
    scopes = ["cloud-platform"]
  }
  metadata_startup_script = "sudo apt update -y; sudo apt install nginx -y"

  metadata = {
    ssh-keys       = "gcedemo:${file("gcedemo.pub")}"
  }

  depends_on = [
    google_compute_router_nat.nat
  ]
}

resource "google_compute_instance" "jump" {
  name         = "jump-host"
  machine_type = "e2-micro"
  zone         = var.zone
  project      = var.project_id

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
  metadata_startup_script = "sudo apt update -y; sudo apt install jq kubectl git -y"
  network_interface {
    subnetwork = data.google_compute_subnetwork.demo_subnet.self_link

    access_config {
      // Ephemeral public IP
    }
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email = google_service_account.asm_install_sa.email
    scopes = ["cloud-platform"]
  }
}

