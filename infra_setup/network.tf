data "google_compute_network" "vpc_network" {
  provider = google-beta
  name = var.network_name

}

data "google_compute_subnetwork" "demo_subnet" {
  provider = google-beta
  name          = "demo-subnet"
}


output "subnet_ranges"{
  value = data.google_compute_subnetwork.demo_subnet.secondary_ip_range
}
