provider "google-beta" {
  project     = var.project_id
  region      = var.region
  zone   = var.zone
}


data "google_project" "project" {
    provider = google-beta
    project_id = var.project_id
}

resource "google_compute_network" "demo_network"{
  provider = google-beta
  name = var.network_name
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "demo_subnet" {
  provider = google-beta
  name          = "demo-subnet"
  ip_cidr_range = "10.2.0.0/16"
  region        = var.region
  network       = google_compute_network.demo_network.id

  secondary_ip_range = [ {
    ip_cidr_range = "10.4.0.0/20"
    range_name = "gke-svcs"
  },
   {
    ip_cidr_range = "192.168.0.0/16"
    range_name = "gke-pod-ips"
  }]

}

resource "google_compute_firewall" "allow_traffic" {
  name = "allow-traffic"
  network = google_compute_network.demo_network.name
  
  allow {
    protocol = "tcp"
    ports = ["22", "80"]
  }
  
  source_ranges = [ "0.0.0.0/0"]
  
}


variable  "region" {}
variable  "zone" {}
variable  "project_id" {}
variable  "network_name" { default = "demo-network" }
