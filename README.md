# gke-ip-reduce

This project explores two options of limiting the required visible ip addresses for a GKE cluster. 

One uses GKE's IP Masqerading, while the other uses Anthos Service Mesh Egress Gateways.

Both versions use a private cluster, a jump-server with a public ip to use for cluster api access and to ssh into a private server with nginx installed. (init scripts are used to install nginx and required tooling on the jumpbox) Setup of the basic infra is supplied in terraform configs. 

**Note** these configs expect version 4.0.0 of the GCP terraform provider.

Once the clusters and components are configured a curl request from a cluster pod (using nginx image as it has curl installed) to the nginx vm is made. Then the user can check the /var/log/nginx/access.log file to see how the source ip reported.

**Notes** 
* To install the components on the private nginx server a Cloud Nat is configured by the terraform scripts
* The terraform configuration in tf directory expects an existing vpc network named 'demo-network' and a subnet named 'demo-subnet' with secondary ranges setup prior to using that config.  The terraform network config supplied in the separate `network_setup` directory. This includes a wide open firewall rule for ports 22 and 80 that your org policy config may delete. (For GCP CEs environment policy allows SSH to public instance VMs from the cloud console)

* The terraform config expects a local public and private key file named gcedemo in the tf directory. You will need to generate this key pair using ssh-keygen or other tooling. This is used to ssh into the private nginx instance from the jump host. After uploading the private key to the jump-host.

* Due to the way secondary ranges are returned from data sources in terraform (in the order they were created) this assumes the pod secondary range is the second secondary range created (The network config accounts for this)

**To use network setup**

1. Navigate to the `/network_setup` directory
2. Update terraform.tfvars to reflect the project, regions, and zones.
3. Execute `>terraform init`
4. Execute `>terraform apply` to create resources

**Common Setup Instructions**
1. Create the gcedemo public and private keys and place in the infra_setup directory
1. Navigate to the `/infra_setup` directory
1. Update terraform.tfvars to reflect the project, regions, and zones.
1. Execute `>terraform init`
1. Execute `>terraform apply` to create resources
1. ssh into the jump server (use ssh link from compute engine VM instances console page)
1. Copy the gcedemo private key to the jump-server (you can use the upload option from the browser ssh window)
1. change the permissions on the gcedemo private key file

    `chmod 600 gcedemo`
1. get the GKE cluster credentials:
  
    `gcloud container clusters get-credentials demo-cluster --zone ZONE`

## IP Masqerading Demonstration
In order to simplify the installation of the ipip-masq-agent daemon set, we will be using a pod ip range of 192.168.0.0/16. This will trigger the automatic installation of the daemon set with the default rules config discussed here - https://cloud.google.com/kubernetes-engine/docs/how-to/ip-masquerade-agent. If you set

**To setup**
1. Perform common setup instructions (see above)
1. (Optional) If you did not use  192.168.0.0/16 as pod ip range install the ip-masq-agent daemon set as described here - https://cloud.google.com/kubernetes-engine/docs/how-to/ip-masquerade-agent#create_manual
1. Create a file named config and copy the contents for the `config` file in this projects `masquerade` directory. (This overides the default masquerading rules masquerade all but 192.168.0.0/16)
1. Create a config map using the file as follows *(note after application of this file it casn take up to 5 mins for the changes to go into effect)*:

    `kubectl create configmap ip-masq-agent --from-file config --namespace kube-system`

**To test**
1. deploy an nginx pod in default (or other selected) namespace (*note this uses nginx image as it has curl installed*)

    `kubectl run nginx --image nginx`
1. get the private ip address of the nginx vm instance (either from VM Instances console page or gcloud command) 
1. make a curl request from nginx pod to the nginx vm

    `kubectl exec -it nginx -- curl NGINX_VM_INTERNAL_IP`
1. Check the access logs to see the request source. (It should be the internal ip address of a gke cluster node)

    `ssh -i gcedemo gcedemo@NGINX_VM_INTERNAL_IP 'tail /var/log/nginx/access.log'`


## Anthos Service Mesh Gateway 

This version uses an Anthos Service Mesh to configure so traffic leaving the cluster would be directed to an egress gateway and that pod IP would be the source of the request to the external world. This would be useful where a node pool with a small pod IP address space would be created and through taints and tolerations only egress gateway pods would be scheduled there. 

**NOTE** - The config files here demostrate using a domain I can control DNS entries on (ripka.dev). To use egress gateways you must be able to configure a dns entry to point to your private nginx server ip. (Alternatively you can use a dns forwarding service like https://nip.io)


**To setup**
1. Perform common setup instructions (see above)
1. (Optional) - if reusing the above cluster, delete the masquerading config map to restore the default rules, which will not masquerade 10.0.0.0/8 traffic.

    `kubectl delete configmap ip-masq-agent --namespace kube-system`
1. (Optional) - if reusing the above cluster, delete the nginx pod

    `kubectl delete pod nginx -n NAMESPACE`
1. Create a DNS entry pointing to your nginx vm ip address (e.g. chopper.ripka.dev)
1. Install asm using managed control plane. see  - https://cloud.google.com/service-mesh/docs/unified-install/managed-service-mesh (*note can also use in cluster control plane*)
1. Ensure injection happens on default (or other selected) namespace. See - https://cloud.google.com/service-mesh/docs/unified-install/managed-service-mesh#deploy_applications

    `kubectl label namespace NAMESPACE istio-injection- istio.io/rev=asm-managed-rapid --overwrite`
1. create a namespace for the gateway using `egw` for this example

    `kubectl create ns egw`
1. label the egress gateway for injection

    `kubectl label namespace egw istio-injection- istio.io/rev=asm-managed-rapid --overwrite`
1. deploy egress gatway (we'll use the setup in the Anthos Service Mesh Samples directory - in this case it is in demo-cluster directory)

    `kubectl apply -n egw demo-cluster/samples/gateways/istio-egressgateway`
1. Copy the content of files in `/asm_gatway` directory into the jump-server
1. Modified copy files for your dns entry
1. create egress gateway and destination rule
    `kubectl apply -f gw.yaml`
1. create service entry
    `kubectl apply -f se.yaml`
1. create virtual service 
    `kubectl apply -f vs.yaml`

**To test**
1. deploy an nginx pod in default (or other selected) namespace (*note this uses nginx image as it has curl installed*)

    `kubectl run nginx --image nginx`
1. get the pod ip address of the egress gateway pods.

    `kubectl get po -n egw -o wide`
1. make a curl request from nginx pod to the nginx vm

    `kubectl exec -it nginx -- curl NGINX_DNS_NAME`
1. Check the access logs to see the request source. (It should be the internal ip address of a egress gateway pod in your egress gateway namespace)

    `ssh -i gcedemo gcedemo@NGINX_VM_INTERNAL_IP 'tail /var/log/nginx/access.log'`